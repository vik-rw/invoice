import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  private apiUrl = '/api/customers';
  data: any = [];

  constructor(private http: Http) { 
    this.getCustomers();
  }
  getCustomers() {
    return this.http.get(this.apiUrl).map((res: Response) => res.json()).subscribe(data => {
      console.log(data);
      this.data = data;
    })
  }

  ngOnInit() {
  }

}
