import { Routes } from '@angular/router';

import { InvoiceComponent } from '../invoice/invoice.component';
import { CustomersComponent } from '../customers/customers.component';
import { ProductsComponent } from '../products/products.component';

export const routes: Routes = [
    {path: 'product', component: ProductsComponent},
    {path: 'customer', component: CustomersComponent},
    {path: 'invoice', component: InvoiceComponent},
    {path: '', redirectTo: 'home', pathMatch: 'full'}
];