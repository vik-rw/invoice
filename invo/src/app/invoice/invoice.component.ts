import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  private apiUrl = '/api/invoices';
  data: any = [];

  constructor(private http: Http) {
    // console.log('Hello fellow user');
    this.getInvoice();
    // this.getData();
   }
  //  getData() {
  //    return this.http.get(this.apiUrl)
  //     .map((res: Response) => res.json())
  //  }
   getInvoice() {
     return this.http.get(this.apiUrl).map((res: Response) => res.json()).subscribe(data => {
       console.log(data);
       this.data = data;
     })
   }

  ngOnInit() {
  }

}
