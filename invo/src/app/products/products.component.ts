import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  prodForm: FormGroup;
  // name: AbstractControl;
  // price: AbstractControl;

  constructor(fb: FormBuilder) {
    this.prodForm = fb.group({
      'name': [null, Validators.required],
      'price': [null, Validators.required]
    });

    // this.name = this.prodForm.controls['name'];
    // this.price = this.prodForm.controls['price'];
   }

  ngOnInit() {
  }
  onSubmit(value: string): void {
    console.log(`your submitted values are: ${value}`);
  }
}
